<?php

class SimpleCartCheckoutFormExtension extends DataExtension {

	public function updateForm(&$fields, &$actions, &$validator) {
		$ShippableProducts = Controller::curr()->data()->SimpleCartProducts()->filter('Shippable', true)->Count();

		$DeliveryFieldName = 'DeliveryOption';
		$fields->removeByName($DeliveryFieldName);

		if ($ShippableProducts) {
			 //massage in the new form field
			 //need to take care of the value too as we call this after form construct


			//$DeliveryFieldOptions = array(
			//		'EmailOnly' => 'Send to my email address',
			//		'EmailAndPost' => 'Post the vouchers to my street address too'
			//);
			//$DeliveryFieldDefaultValues = array_keys($DeliveryFieldOptions);
			//$DeliveryFieldDefaultValue = reset($DeliveryFieldDefaultValues);
			//$DeliveryFieldValue = $this->owner->getController()->getRequest()->postVar($DeliveryFieldName);
			//$data = Session::get("FormInfo.{$this->owner->FormName()}.data");
			//if($DeliveryFieldValue) {
			//	$data[$DeliveryFieldName] = $DeliveryFieldValue;
			//	Session::set("FormInfo.{$this->owner->FormName()}.data", $data);
			//}
			//$DeliveryFieldValue = isset($data[$DeliveryFieldName]) ? $data[$DeliveryFieldName] : $DeliveryFieldDefaultValue;
			//$fields->insertAfter(
			//		$DeliveryField = OptionsetField::create(
			//				$DeliveryFieldName,
			//				'How would you like the tickets delivered?',
			//				$DeliveryFieldOptions,
			//				$DeliveryFieldValue,
			//				$DeliveryFieldDefaultValue
			//		),
			//		'Email'
			//);

			// wrap address fields in display logic
			// plus adjust validator based on above value
			//$AddressFields = array(
			//		'Phone',
			//		'Street',
			//		'Suburb',
			//		'PostCode',
			//		'City',
			//		'Region',
			//		'Country',
			//);
			//$AddressField = DisplayLogicWrapper::create();
			//foreach ($AddressFields as $FieldName) {
			//	$field = $fields->dataFieldByName($FieldName);
			//	$fields->removeByName($FieldName);
			//	$AddressField->push($field);
			//	if($DeliveryFieldValue != 'EmailAndPost') {
			//		$validator->removeRequiredField($FieldName);
			//	}
			//}
			//$AddressField->displayIf($DeliveryFieldName)->isEqualTo('EmailAndPost')->end();
			//$fields->insertAfter($AddressField, $DeliveryFieldName);
		}
	}
}