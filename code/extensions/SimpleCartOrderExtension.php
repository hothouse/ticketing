<?php

class SimpleCartOrderExtension extends DataExtension {

	private static $db = array(
		'DeliveryOption' => 'Varchar'
	);

	public function onCompletePayment() {
		// create voucher codes
		$OrderedProducts = $this->owner->OrderedProducts();
		foreach ($OrderedProducts as $OrderedProduct) {
			$Vouchers = Voucher::get()->filter('OrderedProductID', $OrderedProduct->ID);
			$VouchersToCreate = $OrderedProduct->Quantity - $Vouchers->Count();
			for ($i = 0; $i < $VouchersToCreate; $i++) {
				Voucher::create_new($OrderedProduct);
			}
		}
	}

	public function updateOrderEmail(&$Email) {
		// attach vouchers to email
		$OrderedProducts = $this->owner->OrderedProducts();
		foreach ($OrderedProducts as $OrderedProduct) {
			if ($OrderedProduct->SimpleCartProduct()->IsTicket) {
				foreach ($OrderedProduct->Vouchers() as $Voucher) {
					$Email->attachFileFromString(
							$Voucher->FileContents(),
							$Voucher->FileName()
					);
				}
			}
		}
	}

	public function updateCMSFields(FieldList $fields) {
		$OrderedProducts = $this->owner->OrderedProducts();
		$VoucherCodes = array();
		foreach ($OrderedProducts as $OrderedProduct) {
			foreach ($OrderedProduct->Vouchers() as $Voucher) {
				$VoucherCodes[] = sprintf(
					'%s - %s - <a href="%s">Download</a> - <a href="%s" target="_blank">Manage</a>',
					$Voucher->Code,
					$Voucher->Used ? 'USED' : 'UNUSED',
					$Voucher->DownloadLink(),
					$Voucher->VerificationLink()
				);
			}
		}

		$fields->addFieldToTab(
			'Root.Vouchers',
			LiteralField::create('VoucherCodes', implode('<br>', $VoucherCodes))
		);
	}

}