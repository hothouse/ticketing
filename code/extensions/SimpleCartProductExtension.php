<?php

class SimpleCartProductExtension extends DataExtension {

	private static $db = array(
		'Shippable' => 'Boolean',
		'IsTicket' => 'Boolean'
	);

	private static $has_one = array(
		'VoucherPDF' => 'File'
	);

	private static $defaults = array(
		'Shippable' => true
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->removeByName('SimpleCartPageID');
		$fields->addFieldToTab('Root.Main', CheckboxField::create('Shippable'), 'VoucherPDF');
		$isTicket = CheckboxField::create('IsTicket');
		$fields->addFieldToTab('Root.Main',$isTicket, 'VoucherPDF');

		$VoucherField = DisplayLogicWrapper::create();
		$voucherPdf = $fields->dataFieldByName('VoucherPDF');
		$fields->removeByName('VoucherPDF');
		$VoucherField->push($voucherPdf);
		$VoucherField->hideUnless('IsTicket')->isChecked()->end();
		$fields->addFieldToTab('Root.Main', $VoucherField);
	}
}