<?php

class SimpleCartOrderedProductExtension extends DataExtension {

	private static $has_many = array(
		'Vouchers' => 'Voucher'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->removeByName('Vouchers');
	}

}