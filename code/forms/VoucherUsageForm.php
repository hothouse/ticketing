<?php

class VoucherUsageForm extends Form {

	private static $allowed_actions = array('toggleUsage');

	public function __construct($controller, $name) {
		if ($Voucher = $controller->getVoucher()) {
			$fields = FieldList::create(
				HiddenField::create('Voucher', $Voucher->Code, $Voucher->Code),
				LiteralField::create(
					'Hint',
					sprintf(
						'<p class="lead">Voucher is <strong>%s</strong></p>',
						$Voucher->Used ? 'USED' : 'UNUSED'
					)
				)
			);
			if ($Voucher->LastEdited != $Voucher->Created) {
				$fields->push(
					LiteralField::create(
						'Hint2',
						sprintf(
							'<p class="lead">Last change of usage on %s</p>',
							$Voucher->obj('LastEdited')->Nice()
						)
					)
				);
			}

			// mark unused only for admins
			$actions = FieldList::create();
			if(!$Voucher->Used || Permission::check('VOUCHERMANAGEMENT')) {
				$actions->push(
					FormAction::create('toggleUsage', $Voucher->Used ? 'Mark as unused' : 'Mark as used')
				);
			}
		} else {
			$fields = FieldList::create();
			$actions = FieldList::create();
		}
		parent::__construct($controller, $name, $fields, $actions);
	}

	public function toggleUsage($data, $form) {
		if ($Voucher = $this->getController()->getVoucher()) {
			$Voucher->Used = !$Voucher->Used;
			$Voucher->write();
			$form->sessionMessage(
				sprintf('Voucher has been marked as <strong>%s</strong> successfully.', $Voucher->Used ? 'USED' : 'UNUSED'),
				'success',
				false
			);
		}
		return $this->getController()->redirectBack();

	}
}