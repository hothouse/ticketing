<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 15/11/15
 * Time: 2:17 PM
 */
class TicketAdmin extends ModelAdmin {
	private static $managed_models = array(
		'Event',
		'EventSessionOffer',
		'EventSession',
		'EventSessionLocation',
		'TicketCategory'
	);

	private static $url_segment = 'ticketing';

	private static $menu_title = 'Ticketing';
}