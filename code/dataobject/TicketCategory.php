<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 15/11/15
 * Time: 2:23 PM
 */
class TicketCategory extends DataObject {
	private static $db = array(
		'Name' => 'Varchar(255)',
		'Price' => 'Currency'
	);

	private static $has_many = array(
		'Vouchers' => 'Voucher'
	);

	private static $belongs_many_many = array(
		'EventSessionOffers' => 'EventSessionOffer'
	);
}