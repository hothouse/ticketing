<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 15/11/15
 * Time: 1:50 PM
 */
class Event extends DataObject {
	private static $db = array(
		'Name' => 'Varchar(255)'
	);

	private static $has_many = array(
		'EventSessions' => 'EventSession'
	);

	private static $searchable_fields = array(
		'Name'
	);
}