<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 15/11/15
 * Time: 2:00 PM
 */
class EventSession extends DataObject {

	private static $db = array(
		'Name' => 'Varchar(255)',
		'Time' => 'Datetime'
	);

	private static $has_one = array(
		'Event' => 'Event',
		'Location' => 'EventSessionLocation',
		'EventSessionOffer' => 'EventSessionOffer'
	);

	private static $many_many = array(
		'Tickets' => 'Voucher'
	);

	private static $many_many_extraFields = array(
		'Tickets' => array(
			'Used' => 'Boolean'
		)
	);

	private static $searchable_fields = array(
		'Name',
		'Time'
	);
}