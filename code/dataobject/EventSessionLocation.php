<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 15/11/15
 * Time: 2:05 PM
 */
class EventSessionLocation extends DataObject {

	private static $db = array(
		'Name' => 'Varchar(255)',
	);

	private static $has_many = array(
		'EventSession' => 'EventSession'
	);

	private static $searchable_fields = array(
		'Name'
	);
}