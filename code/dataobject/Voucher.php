<?php

class Voucher extends DataObject implements PermissionProvider {

	private static $db = array(
		'Code' => 'Varchar',
		'Value' => 'Money',
		'Used' => 'Boolean'
	);

	private static $has_one = array(
		'OrderedProduct' => 'SimpleCartOrderedProduct',
		'TicketCategory' => 'TicketCategory'
	);

	private static $searchable_fields = array(
			'Code'
	);

	public function VerificationLink() {
		if ($Page = VoucherPage::get()->first()) {
			return $Page->AbsoluteLink('?Voucher='.$this->Code);
		}
	}

	public function DownloadLink() {
		if ($Page = VoucherPage::get()->first()) {
			return $Page->AbsoluteLink('download/?Voucher='.$this->Code);
		}
	}

	public function FileContents() {
		// check if file exists
		$VoucherFilePath = getTempFolder().DIRECTORY_SEPARATOR.$this->FileName();
		if (!file_exists($VoucherFilePath)) {

			// qr
			$QRFilePath = getTempFolder().DIRECTORY_SEPARATOR.'qr-'.$this->Hash().'.png';
			$renderer = new \BaconQrCode\Renderer\Image\Png();
			$renderer->setHeight(512);
			$renderer->setWidth(512);
			$writer = new \BaconQrCode\Writer($renderer);
			$writer->writeFile($this->VerificationLink(), $QRFilePath);

			// get pdf to print on
			$PDFFilePath = SiteConfig::current_site_config()->PdfPath;

			// command
			$command = implode(
				' ',
				array(
					Config::inst()->get('Voucher', 'convert_path'),
					'-resize "2480x3508^" -gravity center -crop 2480x3508+0+0 +repage',
					escapeshellarg($PDFFilePath),
					'-page +1770+1260 '.escapeshellarg($QRFilePath),
					sprintf(
						'-page +650+1470 -density 120 -pointsize 70 -background transparent label:\'%s\'',
						$this->Code
					),
					/*sprintf(
						'-page +400+400 -density 120 -pointsize 20 -background transparent label:\'%s\'',
						$this->obj('Value')->Nice()
					),*/
					' -layers flatten',
					escapeshellarg($VoucherFilePath)
				)
			);

			// execute
			self::execute($command);
		}

		// return contents
		return file_get_contents($VoucherFilePath);
	}

	public static function execute($Command) {
		$OutPut = array();
		exec($Command.' 2>&1', $OutPut);

		return $OutPut;
	}

	public function Hash() {
		return md5($this->ID.$this->Created.$this->Code);
	}

	public function FileName() {
		return 'voucher-'.$this->Hash().'.jpg';
	}

	public static function create_new($OrderedProduct) {
		$i = 0;
		while (true) {
			$MaxLength = 8;
			$i++;
			if ($i > 100000) {
				$MaxLength++;
			}
			$Code = self::random_code($MaxLength);
			if (Voucher::get()->filter('Code', $Code)->Count() == 0) {
				$Voucher = Voucher::create();
				$Voucher->Code = $Code;
				$Voucher->OrderedProductID = $OrderedProduct->ID;
				$Voucher->ValueAmount = $OrderedProduct->obj('Price')->getAmount();
				$Voucher->ValueCurrency = $OrderedProduct->obj('Price')->getCurrency();
				$Voucher->write();
				return $Voucher;
			}
		}
	}

	private static function random_code($MaxLength) {
		$Chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789';
		$CharsLength = strlen($Chars);
		$Code = '';
		while ($MaxLength--) {
			$Code .= $Chars{mt_rand(0, $CharsLength - 1)};
		}
		return $Code;
	}

	public function canView($member = null) {
		if (Permission::checkMember($member, "VOUCHERMANAGEMENT")) {
			return true;
		}
		return false;
	}

	public function canCreate($member = null) {
		return false;
	}

	public function canEdit($member = null) {
		return false;
	}

	public function canDelete($member = null) {
		return false;
	}

	public function providePermissions() {
		return array(
			'VOUCHERMANAGEMENT' => 'Manage Vouchers'
		);
	}
}