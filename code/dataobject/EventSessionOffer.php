<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 15/11/15
 * Time: 3:09 PM
 */
class EventSessionOffer extends DataObject {
	private static $db = array(
		'Name' => 'Varchar(255)'
	);

	private static $has_many = array(
		'EventSessions' => 'EventSession'
	);

	private static $many_many = array(
		'TicketCategories' => 'TicketCategory'
	);
}