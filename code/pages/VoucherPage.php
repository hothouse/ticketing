<?php

class VoucherPage extends Page {

}

class VoucherPage_Controller extends Page_Controller {

	private static $allowed_actions = array('VoucherUsageForm', 'download');

	public function getVoucher() {
		$Code = $this->getRequest()->requestVar('Voucher');
		$Voucher = Voucher::get()->filter('Code', $Code)->first();
		return $Voucher;
	}

	public function index() {
		//if (!Permission::check('VOUCHERMANAGEMENT')) {
		//	return Security::permissionFailure();
		//}

		if ($Voucher = $this->getVoucher()) {
			return $this->customise(
				array(
					'Voucher' => $Voucher
				)
			);
		}

		return $this;
	}

	public function download() {
		if (!Permission::check('VOUCHERMANAGEMENT')) {
			return Security::permissionFailure();
		}
		$Voucher = $this->getVoucher();
		if ($Voucher && !$Voucher->Used) {
			return SS_HTTPRequest::send_file($Voucher->FileContents(), $Voucher->FileName());
		}
		return $this->redirectBack();
	}

	public function VoucherUsageForm() {
		return VoucherUsageForm::create($this, 'VoucherUsageForm');
	}

}