<div class="container content">
	<div class="row content form">
		<div class="col-xs-12 text-center">
			<% if $Voucher %>
				<h2>
					$Voucher.Code
				</h2>
				<h3>
					$Voucher.Value.Nice
				</h3>
				$VoucherUsageForm
			<% else %>
				<h2>
					Voucher does not exist
				</h2>
			<% end_if %>
		</div>
	</div>
</div>
