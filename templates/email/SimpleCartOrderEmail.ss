<style>
	td, th {
		padding: 8px 5px 12px 5px;
		margin: 0;
		border: 0;
		border-bottom: 1px solid #333;
	}

	th {
		text-align: left;
		background-color: #eeeeee;
		font-weight: bold;
	}
</style>
<% with $Order %>
	<p>
		<% if $FirstName %>
			Hi $FirstName,
		<% else %>
			Hi there,
		<% end_if %>
	</p>	<p>
		We have received your order. Please find below your order details: </p>

	<% if $DeliveryOption == 'EmailOnly' %>
		<p>
			Your details are:
			<br/>
			$FirstName $Surname
			<br/>
			$Email
		</p>
	<% else %>
		<p>
			Your shipping address is:
			<br/>
			$FirstName $Surname
			<br/>
			$Email
			<br/>
			$Phone
			<br/>
			$Street
			<% if $Suburb %>
				<br/>
				$Suburb
			<% end_if %>
			<br/>
			$PostCode $City
			<br/>
			$Region $Country
		</p>
	<% end_if %>

	<table class="table">
		<thead>
			<tr>
				<th class="item-name text-left">Product</th>
				<th class="item-quantity text-center">Qty</th>
				<th class="item-total text-right">SubTotal</th>
			</tr>
		</thead>
		<tbody>
			<% loop $OrderedProducts %>
				<tr>
					<td class="item-name text-left">
						<strong>$Title</strong>
					</td>
					<td class="item-quantity text-center">$Quantity</td>
					<td class="item-total text-right">$Price.Nice</td>
				</tr>
			<% end_loop %>
		</tbody>
		<tfoot>
			<tr>
				<th class="text-left">Total</th>
				<th>&nbsp;</th>
				<th class="total text-right">$Total.Nice</th>
			</tr>
			<% if $ShippingCost.Amount %>
				<tr>
					<td class="item-name text-left">
						Shipping
					</td>
					<td class="item-quantity text-center">&nbsp;</td>
					<td class="item-total text-right">$ShippingCost.Nice</td>
				</tr>
				<tr>
					<th class="text-left">Grand Total</th>
					<th>&nbsp;</th>
					<th class="total text-right">$GrandTotal.Nice</th>
				</tr>
			<% end_if %>
		</tfoot>
	</table>	<p>
		Thanks! </p>
<% end_with %>